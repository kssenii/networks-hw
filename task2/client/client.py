
import json
import time
import socket
import copy
import os

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client_socket.settimeout(1.0)

addr = ("server", 12000)

current_message_idx = 0
first_message = True
timeout = 1000000

filename = os.environ.get('MESSAGES_FILE', 'messages.txt')
client_messages = []

with open(filename) as f:
    client_messages = f.readlines()

def pack(prepared_data):
    return json.dumps(prepared_data).encode("ascii")

def unpack(prepared_data):
    return json.loads(prepared_data)

def send_new_message(message_id):
    global current_message_idx, first_message, client_messages
    if current_message_idx == 0 and not first_message:
        time.sleep(5)
    message = client_messages[current_message_idx]
    current_message_idx = (current_message_idx + 1) % len(client_messages)
    send_message(message, message_id)
    return message

def send_message(message, message_id):
    prepared_data = {'message' : message, 'id' : message_id}
    packed_data = pack(prepared_data)
    client_socket.sendto(packed_data, addr)
    return message

batch = int(os.environ.get('WINDOW_SIZE', 10))
hold = dict()

def log(data):
    unpacked_data = unpack(data)
    message_id = unpacked_data['ACK']
    print(f'Client received ACK from server for: {message_id}')
    delete_from_hold(message_id)
    return message_id

def delete_from_hold(message_id):
    if message_id in hold:
        hold.pop(message_id)

def try_accept_ack(message, message_id):
    try:
        data, server = client_socket.recvfrom(timeout)
    except:
        print(f'Message {message_id} delay')
        return False
    if data is not None:
        received_id = log(data)
        if received_id == message_id:
            return True
        else:
            delete_from_hold(received_id)
    hold[message_id] = sent_message
    return False

message_id = 0
while True:
    if len(client_messages) == 0:
        print('No messages to send, terminating...')
        break
    message_id += 1
    sent_message = send_new_message(message_id)
    try_accept_ack(send_message, message_id)
    if len(hold) > 0:
        while len(hold) > 0:
            hold_copy = hold.copy()
            for id in hold_copy:
                send_message(hold[id], id)
                try_accept_ack(hold[id], id)
    first_message = False
