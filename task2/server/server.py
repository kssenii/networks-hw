
import json
import random
import socket

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind(("server", 12000))

def pack(prepared_data):
    return json.dumps(prepared_data).encode("ascii")

def unpack(prepared_data):
    return json.loads(prepared_data)

def ack(message_id, address):
    answer = {'ACK' : int(message_id)}
    server_socket.sendto(pack(answer), address)

next_message_id = 1
hold = dict()

while True:
    data, address = server_socket.recvfrom(1024)
    unpacked_data = unpack(data)
    message_id = unpacked_data['id']
    message = unpacked_data['message']
    if message_id == next_message_id:
        ack(message_id, address)
        print(f"Prosessing client message: {message} from {address} (id: {message_id})")
        next_message_id += 1
        if address in hold:
            while next_message_id in hold[address]:
                ack(next_message_id, address)
                hold[address].pop(next_message_id)
                next_message_id += 1
        continue
    else:
        if message_id < next_message_id:
            print(f"Got duplicate packet with id: {message_id}")
            continue
        if address in hold:
            hold[address][message_id] = message
        else:
            hold[address] = {message_id : message}
    ack(message_id, address)
