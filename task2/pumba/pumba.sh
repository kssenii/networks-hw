#!/bin/bash

wget https://github.com/alexei-led/pumba/releases/download/0.7.8/pumba_linux_amd64 -O pumba
chmod a+x pumba

./pumba --version

echo "Test with network delay"
./pumba netem --duration 10s delay --time 3000 task2_server_1, task2_client_1
sleep 15s

echo "Test with netwrok loss"
./pumba netem --duration 10s loss --percent 50 task2_server_1, task2_client_1
sleep 15s

echo "Test with packet duplicates"
./pumba netem --duration 10s duplicate --percent 50 task2_server_1, task2_client_1
sleep 15s

echo "Test with packet corruption"
./pumba netem --duration 10s duplicate --percent 50 task2_server_1, task2_client_1
sleep 15s

echo "Test with packet limit"
./pumba netem --duration 10s rate task2_server_1, task2_client_1
sleep 15s
