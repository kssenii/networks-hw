#!/usr/bin/python3

import json
import time
import socket
import copy
import os

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client_socket.settimeout(1.0)

addr = ("127.0.0.1", 12000)

def pack(prepared_data):
    return json.dumps(prepared_data).encode("ascii")

def unpack(prepared_data):
    return json.loads(prepared_data)

def send_new_message(message_id):
    message = input('Next message: ')
    send_message(message, message_id)
    return message

def send_message(message, message_id):
    prepared_data = {'message' : message, 'id' : message_id}
    packed_data = pack(prepared_data)
    client_socket.sendto(packed_data, addr)
    return message

batch = int(os.environ.get('WINDOW_SIZE', 10))
hold = dict()

def log(data):
    unpacked_data = unpack(data)
    message_id = unpacked_data['ACK']
    print(f'Got ACK for: {message_id}')
    delete_from_hold(message_id)
    return message_id

def delete_from_hold(message_id):
    if message_id in hold:
        hold.pop(message_id)

def try_accept_ack(message, message_id):
    data, server = client_socket.recvfrom(1024)
    if data is not None:
        received_id = log(data)
        if received_id == message_id:
            return True
        else:
            delete_from_hold(received_id)
    hold[message_id] = sent_message
    return False

message_id = 0
while True:
    message_id += 1
    sent_message = send_new_message(message_id)
    try_accept_ack(send_message, message_id)
    if len(hold) > 0:
        while len(hold) > 0:
            hold_copy = hold.copy()
            for id in hold_copy:
                send_message(hold[id], id)
                try_accept_ack(hold[id], id)

